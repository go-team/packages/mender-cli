module github.com/mendersoftware/mender-cli

go 1.14

require (
	github.com/cheggaaa/pb/v3 v3.1.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	github.com/howeyc/gopass v0.0.0-20190910152052-7cb4b85ec19c
	github.com/mendersoftware/go-lib-micro v0.0.0-20210322083710-42186ab64a1e
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.5.0
	github.com/spf13/viper v1.13.0
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
)
